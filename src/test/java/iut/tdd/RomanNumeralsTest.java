package iut.tdd;

import org.junit.Assert;
import org.junit.Test;

public class RomanNumeralsTest {

	@Test
	public void should_return_I_when_1() {
		// Given
		String input = "1";
		String expected = "I";
		RomanNumerals romanNumeral = new RomanNumerals();
		// When
		String actual = romanNumeral.convertToRoman(input);
		// Then
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void should_return_III_when_3() {
		// Given
		String input = "3";
		String expected = "III";
		RomanNumerals romanNumeral = new RomanNumerals();
		// When
		String actual = romanNumeral.convertToRoman(input);
		// Then
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void should_return_VI_when_6() {
		// Given
		String input = "6";
		String expected = "VI";
		RomanNumerals romanNumeral = new RomanNumerals();
		// When
		String actual = romanNumeral.convertToRoman(input);
		// Then
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void should_return_XXXVII_when_37() {
		// Given
		String input = "37";
		String expected = "XXXVII";
		RomanNumerals romanNumeral = new RomanNumerals();
		// When
		String actual = romanNumeral.convertToRoman(input);
		// Then
		Assert.assertEquals(expected, actual);
	}
	
	

	@Test
	public void should_return_1_when_I() {
		// Given
		String input = "I";
		String expected = "1";
		RomanNumerals romanNumeral = new RomanNumerals();
		// When
		String actual = romanNumeral.convertFromRoman(input);
		// Then
		Assert.assertEquals(expected, actual);
	}
}
