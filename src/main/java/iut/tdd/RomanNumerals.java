package iut.tdd;

import java.util.HashMap;

public class RomanNumerals {
	public String convertToRoman(String arabe) {
		int i = Integer.parseInt(arabe);
		StringBuilder resultat = new StringBuilder();
		HashMap<String, Integer> symbolsValues = new HashMap<String, Integer>();
		symbolsValues.put("V", 5);
		symbolsValues.put("X", 10);
		symbolsValues.put("L", 50);

		String[] orderedSymbols = { "X", "V", "L" };

		for (String symbol : orderedSymbols) {
			i = extractSymbol(i, resultat, symbolsValues, symbol);
		}

		while (i != 0) {
			resultat.append("I");
			i = i - 1;
		}

		return resultat.toString();
	}

	private int extractSymbol(int i, StringBuilder resultat, HashMap<String, Integer> symbolsValues, String symbol) {
		while (i % symbolsValues.get(symbol) != i) {
			resultat.append(symbol);
			i = i - symbolsValues.get(symbol);
		}
		return i;
	}

	public String convertFromRoman(String roman) {

		int conversion = 0;
		String resultat;
		for (int i = 0; i < roman.length(); i++) {
			if (roman.charAt(i) == 'X') {
				conversion = conversion + 10;
			} else if (roman.charAt(i) == 'V') {
				conversion = conversion + 5;
			} else if (roman.charAt(i) == 'I') {
				conversion = conversion + 1;
			}
		}

		resultat = "" + conversion;

		return resultat;
	}

}
